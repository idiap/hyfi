---
title: "Monthly cohort descriptives"
output: html_document
---

```{r, include=FALSE}
library(WriteXLS)
library(dplyr)
library(tidyr)
library(ggplot2)
options(dplyr.print_min = 4L, dplyr.print_max = Inf, digits=5, width=180)
knitr::opts_chunk$set(comment="  ", echo=F, warning=F)

NIMP = 10

v_num = c('age', "talla", "pes", 
          "bp", "tas", "tad", "glucosa", "coltot", "colhdl", "colldl", "tg", "ps")

factors = c('medeaR', 'medeaU1','medeaU2','medeaU3', 'medeaU4', 'medeaU5', 'exitusE', 'exitusT', 'i.ep_aff.all')
meds = c('a10', 'm01', 'g03a', 'statine', 'n06', 'aspirine', 'h02', 'c08', 'c09', 'c10.no.statine','c02', 'c03', 'c07',
         'C10AA01', 'C10AA02', 'C10AA03', 'C10AA04', 'C10AA05', 'C10AA07')
probs_basal = c('diabetes', 'htn', 'dyslipidemia', 'aff', 'smoking', 'obesity', 'ckd', 'neoplasms_malignant', 
                'hf', 'vhd', 'copd')
v_cat = c(factors, meds, probs_basal)

data = lapply(1:NIMP, function(imp){
  load(file=sprintf('%s/data/hyfi-mtch-imp_%02d.RData', PROJ_ROOT, imp))
  data %>% select(user, dintro, one_of(v_num, v_cat)) %>% mutate(imp = imp)
}) %>% bind_rows
```

```{r}
## CONTAINERS
RESULTS_NUM = list()
include_num = function(df, g1 = 'all', g2 = 'all'){
  df.new = df
  df.new$g1 = g1
  if(g1 %in% names(df))
    df.new$g1 = df[,g1]
  df.new$g2 = g2
  if(g2 %in% names(df))
    df.new$g2 = df[,g2]
  RESULTS_NUM[[length(RESULTS_NUM)+1]] <<- df.new[,c('g1', 'g2', 'variable', 'n', 'na', 'na.p', 'mean', 'sd', 'min', 'q1', 'median', 'q3', 'max')]
  df
}
summ_num = function(.data) summarise(.data, 
  n = length(variable),
  na = sum(is.na(value)),
  na.p = (100 * mean(is.na(value))) %>% round(1),
  mean = mean(value, na.rm = T),
  sd = sd(value, na.rm=T),
  min = min(value, na.rm=T),
  q1 = quantile(value, 0.25, na.rm=T),
  median = quantile(value, 0.5, na.rm=T),
  q3 = quantile(value, 0.75, na.rm=T),
  max = max(value, na.rm=T)) %>% data.frame
RESULTS_CAT = list()
include_cat = function(df, g1 = 'all', g2 = 'all'){
  df.new = df
  df.new$g1 = g1
  if(g1 %in% names(df))
    df.new$g1 = df[,g1]
  df.new$g2 = g2
  if(g2 %in% names(df))
    df.new$g2 = df[,g2]
  RESULTS_CAT[[length(RESULTS_CAT)+1]] <<- df.new[,c('g1', 'g2', 'variable', 'n', 'na', 'na.p', 'count', 'prop')]
  df
}
summ_cat = function(.data) summarise(.data, 
  n = length(variable),
  na = sum(is.na(value)),
  na.p = mean(is.na(value)),
  count = sum(value),
  prop = (100*mean(value))) %>% data.frame
```

```{r}
d.num  = data %>% select(user, dintro, one_of(v_num)) %>% gather(key='variable', value='value', -user, -dintro) %>%
  mutate(dintro = as.Date(dintro, origin="1970-01-01"))
d.cat  = data %>% select(user, dintro, one_of(v_cat)) %>% gather(key='variable', value='value', -user, -dintro, convert=T) %>%
  mutate(dintro = as.Date(dintro, origin="1970-01-01"))
```

# Descriptives for all population

  * Numeric descriptives

```{r}
res.num.all = d.num %>% group_by(variable) %>% summ_num %>% include_num
res.num = d.num %>% group_by(variable, dintro) %>% summ_num
```

```{r}
res.num.all
```

```{r, fig.width=12, fig.height=6}
ggplot() + 
  geom_hline(data = res.num.all, aes(yintercept = mean), color='red') +
  geom_point(data = res.num, aes(x = dintro, y = mean)) +
  facet_wrap(~variable, ncol=3, scale='free')
```

  * Categoric descriptives

```{r}
res.cat.all = d.cat %>% group_by(variable) %>% summ_cat %>% include_cat
res.cat = d.cat %>% group_by(variable, dintro) %>% summ_cat
```

```{r}
res.cat.all %>% arrange(variable)
```

```{r, fig.width=12, fig.height=20}
ggplot() + 
  geom_hline(data = res.cat.all, aes(yintercept = prop), color='red') +
  geom_point(data = res.cat, aes(x = dintro, y = prop)) +
  facet_wrap(~variable, ncol=3, scale='free')
```

# Descriptives by user

  * Numeric descriptives
  
```{r}
res.num.all = d.num %>% group_by(variable, user) %>% summ_num %>% include_num(g1 = 'user')
res.num = d.num %>% group_by(variable, user, dintro) %>% summ_num
```

```{r}
res.num.all
```

```{r, fig.width=12, fig.height=6}
ggplot() + 
  geom_hline(data = res.num.all, aes(yintercept = mean, color=user)) +
  geom_point(data = res.num, aes(x = dintro, y = mean, color=user)) +
  facet_wrap(~variable, ncol=3, scale='free')
```

  * Categoric descriptives

```{r}
res.cat.all = d.cat %>% group_by(variable, user) %>% summ_cat %>% include_cat(g1 = 'user')
res.cat = d.cat %>% group_by(variable, user, dintro) %>% summ_cat
```

```{r}
res.cat.all
```

```{r, fig.width=12, fig.height=18}
ggplot() + 
  geom_hline(data = res.cat.all, aes(yintercept = prop, color=user)) +
  geom_point(data = res.cat, aes(x = dintro, y = prop, color=user)) +
  facet_wrap(~variable, ncol=3, scale='free')
```

# Excel linking

* Numeric variables

```
='http://10.81.155.225/projects.thor/hyfi/www/[descriptives-matched.xls]num'!A1
```

* Categoric variables

```
='http://10.81.155.225/projects.thor/hyfi/www/[descriptives-matched.xls]cat'!A1
```

Download <a href="descriptives-matched.xls">xls</a>

```{r}
num = bind_rows(RESULTS_NUM) %>% data.frame
cat = bind_rows(RESULTS_CAT) %>% data.frame
WriteXLS(c('num', 'cat'), ExcelFileName = sprintf('%s/www/descriptives-matched.xls', PROJ_ROOT))
```