---
title: "Variable selection"
output: html_document
---

```{r, include=FALSE}
knitr::opts_chunk$set(comment="  ", echo=F, warning=F)
if(!exists('data') | !is.data.frame(get('data')) ){
  load(sprintf('%s/data/hyfi_2008-01.RData', PROJ_ROOT))
  OFILE = "data/mi-variable-selection_2008-01.RData"
}

OFILE = sprintf('%s/%s', PROJ_ROOT, OFILE)
options(width=120)

library(survival)

devtools::load_all('/home/idiap/lib/R/usrgi_lib')
source(sprintf('%s/R/util_imputation.R', PROJ_ROOT))

v_missing = c("talla", "pes", "lnbp", "lntad", "lnglucosa", "lncoltot", "lncolhdl", "lncolldl", "lntg")

#### Descriptives variables disponibles
v_numeric = c('age', 'na_aff.all')

factors = c('medeaU1','medeaU2','medeaU3', 'medeaU4', 'medeaU5', 'exitusE', 'i.ep_aff.all')
meds = c('iuser', 'a10', 'm01', 'statine', 'n06', 'n05', 'aspirine', 'h02', 'c08', 'c09', 'c10.no.statine','c02', 'c03', 'c07')
probs_basal = c('diabetes', 'htn', 'dyslipidemia', 'smoking', 'obesity', 'ckd', 'neoplasms_malignant', 
                'hf', 'vhd', 'copd') # Pensar en modificar també descriptives-numeric.Rmd

v_factor = c(factors, meds, probs_basal)

v_numeric_common = intersect( names(data), v_numeric)
v_factor_common = intersect( names(data), v_factor)

if( length(v_numeric) != length(v_numeric_common) | length(v_factor) != length(v_factor_common)){
  miss = setdiff( c(v_numeric, v_factor), c(v_numeric_common, v_factor_common) )
  message( sprintf("%s are not available in this table", paste(miss, collapse = ', ')) )
}

v_numeric = v_numeric_common
v_factor = v_factor_common

v_explanatory = c(v_numeric, v_factor)
v_interact = 'sex'
data$user_sex = with(data, interaction(user, sex))
```

Variables to impute:
  
```{r}
v_missing
```

Explanatory variables considered for the imputation process:
  
```{r}
v_explanatory
```

Interaction

```{r}
v_interact
```

Significance levels at `alpha = 0.05`.

```{r}
data.imp = data[, c('ocip', 'dintro', union(v_explanatory, v_missing), v_interact)]

restrictions = list(
  list(v = c('lnbp', 'lntad'), omit = c('lnbp', 'lntad'), model='reg'),
  list(v = c('talla', 'pes'), omit = c('talla', 'pes'), model='reg') ,
  list(v = c('lncoltot', 'lncolhdl', 'lncolldl', 'lntg'), omit = c('lncoltot', 'lncolhdl', 'lncolldl', 'lntg'), model='reg'),
  list(v = c('lnglucosa'), omit = 'lnglucosa', model='reg') )

D.inter = split(data.imp, data.imp[,v_interact])

res = list()
for(lvl in names(D.inter)){
  res[[lvl]] = sum_dependances( D.inter[[lvl]],
                                ignore_vars = c('ocip', 'dintro', v_interact, 'na_aff.all', 'i.ep_aff.all'),
                                restrictions = restrictions )
}


save(data.imp, res, D.inter, file=OFILE)
```


### Matrius de predicció


```{r, comment="   "}
for(inter in names(res)){
  cat(sprintf("\n\n===== %10s ================================================================\n\n", inter))
  mi_pred_matrix_h = lapply(res[[inter]]$vars, function(l) union(l$na, l$value))
  mi_pred_matrix_h = lapply(mi_pred_matrix_h, function(l) union(l, c('i.ep_aff.all', 'na_aff.all', 
                                                                    'age', 'smoking', 'diabetes')))
  for(v in names(mi_pred_matrix_h)){
    cat(sprintf("----- %10s ----------------------------------------------------------------\n", v))
    print(sort(mi_pred_matrix_h[[v]]), quote=F)
  }
}
```


## Detall de les relacions calculades 

#### Relacions amb els valors

```{r, comment="   "}
for(inter in names(res)){
  cat(sprintf("\n\n===== %10s ================================================================\n\n", inter))
  print(res[[inter]]$values)
}
```

#### Relacions amb l'indicadora de missing

```{r, comment="   "}
for(inter in names(res)){
  cat(sprintf("\n\n===== %10s ================================================================\n\n", inter))
  print(res[[inter]]$na)
}
```
