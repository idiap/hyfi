---
title: "Cox models: Hazards adjusted by ps (moving risk level)"
output: html_document
---

```{r, include=FALSE}
if(!exists('PROJ_ROOT')) PROJ_ROOT = "/home/idiap/projects.thor/hyfi"
library(dplyr)
library(survival)
library(dplyr)
library(ggplot2)
library(tidyr)
library(WriteXLS)
library(mice)
library(grid)
library(gridExtra)
knitr::opts_chunk$set(comment="  ", echo=F, warning=F)
NIMP = 10
data = lapply(1:NIMP, function(IMP){
  load(sprintf("%s/data/hyfi-mtch-imp_%02d.RData", PROJ_ROOT, IMP))
  # Es copia l'mpr de l'usuari al control (controls i usuaris estan ordenats)
  data[,'mpr1y'] = rep(data[data$iuser == 1,'mpr1y'] %>% .[['mpr1y']], each=2)
  data[,'aff.event'] = with(data, rep(
    pmin(ifelse(i.ep_aff.all[data$iuser == 1] == 0, Inf, t.ep_aff.all[data$iuser == 1]),
         ifelse(i.ep_aff.all[data$iuser == 0] == 0, Inf, t.ep_aff.all[data$iuser == 0])), each = 2))
  data %>%
    select(ocip, age, dintro, mtch, user, iuser, mpr1y, risk.aff, ps, aff.event, 
           i.ep_aff.all, t.ep_aff.all) %>%
    mutate(risk.aff = 100*risk.aff, imp = IMP)
}) %>% bind_rows

devtools::load_all('/home/idiap/lib/R/usrgi_lib')

options(width=150)

fitting_02 = function(age, mtch, dintro, time, event, user, ps, imp, risk){
  df = data.frame('age' = age, 'mtch' = mtch, 'dintro' = dintro, 'time' = time, 'event' = event, 'user' = user, 'ps' = ps, 'imp' = imp, 'risk' = risk)
  nimp = length(unique(df$imp))
  l_df = split(df, df$imp)
  
  suppressWarnings(
    mod <- summary(pool(as.mira(lapply(l_df, with,  eval(parse(text = GLOBAL_FIT_2))))))
  )
  suppressWarnings(
    inc_user <- summary(pool(as.mira(lapply(l_df, with, incidence(event = event[user==1], time = time[user==1], period = 5, pers = 100)))))
  )
  suppressWarnings(
    inc_cntr <- summary(pool(as.mira(lapply(l_df, with, incidence(event = event[user==0], time = time[user==0], period = 5, pers = 100)))))
  )
  
  data.frame('age' = mean(age),
             'risk.aff' = mean(risk),
             'ctrl.n' = sum(user==0)/nimp,
             'ctrl.ev' = sum(event[user==0]/nimp),
             'ctrl.inc' = inc_cntr[1, 'est'],
             'user.n' = sum(user==1)/nimp,
             'user.ev' = sum(event[user==1])/nimp,
             'user.inc' = inc_user[1, 'est'],
             'nnt' = 1/(inc_cntr[1, 'est']/100-inc_user[1, 'est']/100),
             'haz' = exp(mod['user', 'est']), 
             'haz.lo' = exp(mod['user', 'lo 95']), 
             'haz.hi' = exp(mod['user', 'hi 95']))
}
```

```{r}
eps = c('ep_aff.all')

mpr_period = function(days.treated, followup = rep(mpr.months * treat.duration.days, length(env)), 
                      treat.duration.days = 28, mpr.months = 12){
  res = pmin(followup, pmin(days.treated/treat.duration.days, mpr.months) * treat.duration.days) / pmin(followup, mpr.months * treat.duration.days)
  res[followup == 0] = 1
  res
}
d = bind_cols(
  data %>% dplyr::select(user, dintro, age, iuser, risk.aff, ps, imp, mtch, mpr1y, aff.event, one_of(sprintf('i.%s', eps))) %>% 
    gather(key=outcome, value=event, -user, -dintro, -age, -iuser, -risk.aff, -ps, -imp, -mtch, -mpr1y, -aff.event),
  data %>% dplyr::select(one_of(sprintf('t.%s', eps))) %>% 
    gather(key=outcome, value=time) %>%
    select(time)) %>% mutate(
      grisk_aff.4 = cut(risk.aff, breaks = c(0, 2.5, 5, 7.5, 100), include.lowest = T),
      grisk_aff.3a = cut(risk.aff, breaks = c(0, 2.5, 7.5, 100), include.lowest = T),
      grisk_aff.3b = cut(risk.aff, breaks = c(0, 2.5, 5, 100), include.lowest = T),
      mpr = 100*mpr_period(mpr1y, time),
      mpr80 = ifelse(mpr > 80, 'MPR1Y>80', 'MPR1Y≤80')
    )
```


Adjusted model:

```{r, echo=TRUE}
GLOBAL_FIT_2 = "coxph(Surv(time = time, event = event)~strata(dintro)+user+ps)"
```


# Hazards separating the population in groups with similar size

```{r, echo=FALSE}
N_TOTAL = mean(table(data$imp))
stratas = function(n, size,  length.out){
  delta = size/n
  list('delta' = delta/2, 'seq' = seq(delta/2, 1-delta/2, length.out = length.out))
}
NSIZE= 20000
STR = stratas(n = mean(table(data$imp)), size = NSIZE, length.out = 10)

l_df = lapply( STR[[2]], function(quant, delta){
  risk_min = quantile(data$risk.aff, quant - delta)
  risk_max = quantile(data$risk.aff, quant + delta)
  data.frame(h01 <- d %>% subset( risk_min < risk.aff & risk.aff < risk_max  ) %>% 
               group_by(outcome) %>% 
               do(fitting_02(.$age, .$mtch, .$dintro, .$time, .$event, .$iuser, .$ps, .$imp, .$risk.aff))) %>% 
    mutate(quantile = quant, risk_min = risk_min, risk_max = risk_max )
}, STR[[1]])
df2 = bind_rows(l_df)

df2 %>% 
  mutate(
    age = sprintf( "   %3.1f", age),
    user = sprintf("   %5.1f/%7.1f (%4.1f)", user.ev, user.n, user.inc),
    ctrl = sprintf("   %5.1f/%7.1f (%4.1f)", ctrl.ev, ctrl.n, ctrl.inc),
    risk = sprintf("   %4.1f [%3.1f, %4.1f]", risk.aff, risk_min, risk_max),
    haz = sprintf( "   %5.2f [%4.2f, %4.2f]",haz, haz.lo, haz.hi),
    nnt = nnt) %>%
  select(risk, age, ctrl, user, haz, nnt) %>% print(n=Inf)
```

```{r, fig.width=6, fig.height=5, results='hold'}
g1<- ggplot(data = df2) + 
  geom_point(aes(x=risk.aff, y=haz), size=3) + 
  geom_hline(yintercept=1, col='red') + 
  geom_errorbar(aes(x=risk.aff, ymin=haz.lo, ymax=haz.hi), alpha=0.4, size=2) +
  coord_cartesian(ylim=c(0,2)) + 
  theme_classic() + ylab('Hazard') +
  theme(axis.ticks = element_blank(), 
        axis.text.x = element_blank(), 
        axis.line.x = element_blank(),
        axis.title.x = element_blank())

g2 <- ggplot(data = df2) + 
  geom_point(aes(x=risk.aff, y=ctrl.inc, col='control'), size=3) + 
  geom_point(aes(x=risk.aff, y=user.inc, col='user'), size=3) + 
  theme_classic() + ylab('Incidences') + xlab('Estimated risk (mean))') + 
  theme(legend.position = 'bottom')

grid.newpage()
vpa_ <- viewport(width = 1, height = 0.5, x = 0.5, y = 0.75)
vpb_ <- viewport(width = 1, height = 0.5, x = 0.5, y = 0.25)
print(g1, vp = vpa_)
print(g2, vp = vpb_)
```

```{r, fig.width=6, fig.height=7, results='hold'}
RISK_MAX = 15
g1<- ggplot(data = df2) + 
  geom_point(aes(x=risk.aff, y=haz), size=3) + 
  geom_hline(yintercept=1, col='red') + 
  geom_errorbar(aes(x=risk.aff, ymin=haz.lo, ymax=haz.hi), size=1) +
  coord_cartesian(ylim=c(0,2)) + 
  theme_classic() + ylab('Hazard') +
  theme(axis.ticks = element_blank(), 
        axis.text.x = element_blank(), 
        axis.line.x = element_blank(),
        axis.title.x = element_blank()) + coord_cartesian(xlim = c(0, RISK_MAX))

g2 <- ggplot(data = df2) + 
  geom_point(aes(x=risk.aff, y=ctrl.inc, col='control'), size=3) + 
  geom_point(aes(x=risk.aff, y=user.inc, col='user'), size=3) + 
  theme_classic() + ylab('Incidences') + xlab('Estimated risk (mean))') +
  theme(legend.position=c(0.8, 0.6),
        axis.ticks = element_blank(), 
        axis.text.x = element_blank(), 
        axis.line.x = element_blank(),
        axis.title.x = element_blank()) + coord_cartesian(xlim = c(0, RISK_MAX))

formatter <- function(x) sprintf('%2.1f', x/1000) 

d.risk = data %>% group_by(ocip) %>% summarise(risk.aff = mean(risk.aff) )
g3 = ggplot(data = d.risk) + geom_histogram(aes(x=risk.aff), binwidth=0.5) + theme_classic() + 
  scale_y_continuous(label=formatter) + 
  ylab('Population (x1000)') + xlab('Estimated risk') + coord_cartesian(xlim = c(0, RISK_MAX))

plots = list(g1,g2,g3)
grobs <- list()
widths <- list()

for (i in 1:length(plots)){
    grobs[[i]] <- ggplotGrob(plots[[i]])
    widths[[i]] <- grobs[[i]]$widths[2:5]
}

maxwidth <- do.call("unit.pmax", widths)

for (i in 1:length(grobs)){
     grobs[[i]]$widths[2:5] <- as.list(maxwidth)
}

do.call("grid.arrange", c(grobs, ncol = 1, top=sprintf('Results using subsamples of size %d', NSIZE)))
```

```{r}
l_df = lapply( STR[[2]], function(quant, delta){
  risk_min = quantile(data$risk.aff, quant - delta)
  risk_max = quantile(data$risk.aff, quant + delta)
  data.frame(h01 <- d %>% subset( risk_min < risk.aff & risk.aff < risk_max  ) %>% 
               group_by(outcome, mpr80) %>% 
               do(fitting_02(.$age, .$mtch, .$dintro, .$time, .$event, .$iuser, .$ps, .$imp, .$risk.aff))) %>% 
    mutate(quantile = quant, risk_min = risk_min, risk_max = risk_max)
}, STR[[1]])
df = bind_rows(l_df)
```

```{r}
df2 = df %>% subset(mpr80 == 'MPR1Y>80')
df2 %>% 
  mutate(
    age = sprintf( "   %3.1f", age),
    user = sprintf("   %5.1f/%7.1f (%4.1f)", user.ev, user.n, user.inc),
    ctrl = sprintf("   %5.1f/%7.1f (%4.1f)", ctrl.ev, ctrl.n, ctrl.inc),
    risk = sprintf("   %4.1f [%3.1f, %4.1f]", risk.aff, risk_min, risk_max),
    haz = sprintf( "   %5.2f [%4.2f, %4.2f]",haz, haz.lo, haz.hi),
    nnt = nnt) %>%
  select(risk, age, ctrl, user, haz, nnt) %>% print(n=Inf)
```

```{r, fig.width=6, fig.height=7, results='hold'}
g1<- ggplot(data = df2) + 
  geom_point(aes(x=risk.aff, y=haz), size=3) + 
  geom_hline(yintercept=1, col='red') + 
  geom_errorbar(aes(x=risk.aff, ymin=haz.lo, ymax=haz.hi), size=1) +
  coord_cartesian(ylim=c(0,2)) + 
  theme_classic() + ylab('Hazard') +
  theme(axis.ticks = element_blank(), 
        axis.text.x = element_blank(), 
        axis.line.x = element_blank(),
        axis.title.x = element_blank()) + coord_cartesian(xlim = c(0, 30))

g2 <- ggplot(data = df2) + 
  geom_point(aes(x=risk.aff, y=ctrl.inc, col='control'), size=3) + 
  geom_point(aes(x=risk.aff, y=user.inc, col='user'), size=3) + 
  theme_classic() + ylab('Incidences') + xlab('Estimated risk (mean))') + 
  theme(legend.position=c(50,15),
        axis.ticks = element_blank(), 
        axis.text.x = element_blank(), 
        axis.line.x = element_blank(),
        axis.title.x = element_blank()) + coord_cartesian(xlim = c(0, 30))

formatter <- function(x) sprintf('%2.1f', x/1000) 

d.risk = data %>% group_by(ocip) %>% summarise(risk.aff = mean(risk.aff) )
g3 = ggplot(data = d.risk) + geom_histogram(aes(x=risk.aff), binwidth=0.5) + theme_classic() + 
  scale_y_continuous(label=formatter) + 
  ylab('Population (x1000)') + xlab('Estimated risk') + coord_cartesian(xlim = c(0, 30))

plots = list(g1,g2,g3)
grobs <- list()
widths <- list()

for (i in 1:length(plots)){
    grobs[[i]] <- ggplotGrob(plots[[i]])
    widths[[i]] <- grobs[[i]]$widths[2:5]
}

maxwidth <- do.call("unit.pmax", widths)

for (i in 1:length(grobs)){
     grobs[[i]]$widths[2:5] <- as.list(maxwidth)
}

do.call("grid.arrange", c(grobs, ncol = 1, top=sprintf('Results using subsamples of size %d (MPR1Y>80)', NSIZE)))
```

```{r, fig.width=7, fig.height=6}
ggplot(data = df) + 
  geom_point(aes(x=risk.aff, y=haz, col=mpr80), size=3) + 
  geom_hline(yintercept=1, col='red') + 
  geom_errorbar(aes(x=risk.aff, ymin=haz.lo, ymax=haz.hi, col=mpr80), alpha=0.4, size=2) +
  coord_cartesian(ylim=c(0,2)) + 
  theme_bw() + ylab('Hazard') + xlab('Estimated risk (mean))') + theme(legend.position = 'bottom')
```

