I_MONTH = 1 2 3 4 5 6 7 8 9 10 11 12
I_YEAR = 2007 2008 2009 2010 2011 2012
MONTHS = 2006-07 2006-08 2006-09 2006-10 2006-11 2006-12 \
				 $(foreach v,$(I_YEAR),$(foreach w,$(I_MONTH),$(shell printf '%04d-%02d' $(v) $(w)))) \
				 2013-01 2013-02 2013-03 2013-04 2013-05 2013-06
IMP = 1 2 3 4 5 6 7 8 9 10

MONTHLY_RAW = $(foreach m,$(MONTHS),$(shell printf 'data/hyfi_%s.RData' $(m)))
MONTHLY_PS = $(foreach imp,$(IMP), $(foreach m,$(MONTHS),$(shell printf 'data/hyfi-ps-imp_%02d_%s.csv' $(imp) $(m))))

MATCHED_DATA = $(foreach imp,$(IMP),$(shell printf 'data/hyfi-mtch-imp_%02d.RData' $(imp)))
MONTHLY_DATA = $(foreach m,$(MONTHS),$(shell printf 'data/hyfi_%s.csv' $(m))) \
               $(MONTHLY_RAW) \
               $(foreach m,$(MONTHS),$(shell printf 'www/mi-variable-selection_%s.html' $(m))) www/mi-variable-selection_dintro-DD.html \
               $(foreach m,$(MONTHS),$(shell printf 'data/mi-variable-selection_%s.RData' $(m))) \
               $(foreach m,$(MONTHS),$(shell printf 'data/mi-imputation-H_%s.RData' $(m))) \
               $(foreach m,$(MONTHS),$(shell printf 'data/mi-imputation-D_%s.RData' $(m))) \
               $(foreach m,$(MONTHS),$(shell printf 'www/mi-validate-process_%s.html' $(m))) www/mi-validate-process_dintro-DD.html \
               $(foreach imp,$(IMP), $(foreach m,$(MONTHS),$(shell printf 'data/hyfi-imp_%02d_%s.RData' $(imp) $(m)))) \
               $(foreach imp,$(IMP), $(foreach m,$(MONTHS),$(shell printf 'data/hyfi-ps-imp_%02d_%s.RData' $(imp) $(m)))) www/hyfi-ps-imp.dintro_DD.html \
               $(MONTHLY_PS) \
               $(foreach imp,$(IMP),$(shell printf 'data/hyfi-mtch-imp_%02d.csv' $(imp))) \
               $(MATCHED_DATA)

EMPTY :=
SPACE := $(EMPTY) $(EMPTY)
MONTHS_LIST = $(subst $(SPACE),:,$(MONTHS))

RMD2HTML = R/run-knit2html.R

.IPHONY = all raw ps match



all: data/cohort-generation.csv $(MONTHLY_DATA) \
     www/cohort-monthly-descriptives.html www/cohort-monthly-entrances.html \
     www/cox-models-01a-POOL.html www/cox-models-02a-POOL.html www/cox-models-03a-POOL.html \
     www/cox-models-05a-POOL.html www/cox-models-06a-POOL.html www/cox-models-07a-POOL.html www/cohort-monthly-descriptives-matched.html \
     www/cohort-monthly-descriptives-matched.html www/risk_comparision.html

raw: $(MONTHLY_RAW)

ps: $(MONTHLY_PS)

match: $(MONTHLY_DATA)

data/cohort-generation.csv : script.python/cohort-generation.py
	python $< > www/cohort-generation.html

data/hyfi_%.csv : script.python/monthly-generation.py data/cohort-generation.csv
	python $< $* > www/monthly-generation-$*.html

data/hyfi_%.RData : script.R/initialize.R data/hyfi_%.csv R/util_219.R
	Rscript -e 'data = read.csv("data/hyfi_$*.csv", sep=";", stringsAsFactors=FALSE); source("$<"); save(data, file="$@")'

www/mi-variable-selection_%.html : script.Rmd/mi-variable-selection.Rmd data/hyfi_%.RData
	Rscript -e 'OFILE = "data/mi-variable-selection_$*.RData"; load("data/hyfi_$*.RData"); OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'

data/mi-variable-selection_%.RData : www/mi-variable-selection_%.html script.Rmd/mi-variable-selection.Rmd data/hyfi_%.RData
	touch $@

data/mi-imputation-H_%.RData : script.R/mi-imputation.R data/mi-variable-selection_%.RData R/util_imputation.R
	Rscript -e 'INTER = "H"; MONTH = "$*"; source("$<")'

data/mi-imputation-D_%.RData : script.R/mi-imputation.R data/mi-variable-selection_%.RData R/util_imputation.R
	Rscript -e 'INTER = "D"; MONTH = "$*"; source("$<")'

www/mi-validate-process_%.html : script.Rmd/mi-validate-process.Rmd data/mi-imputation-H_%.RData data/mi-imputation-D_%.RData data/mi-variable-selection_%.RData
	Rscript -e 'MI_SEL ="data/mi-variable-selection_$*.RData"; MI_H = "data/mi-imputation-H_$*.RData"; MI_D = "data/mi-imputation-D_$*.RData"; OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'

data/hyfi-imp_01_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=1; MONTH="$*"; source("$<")'
data/hyfi-imp_02_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=2; MONTH="$*"; source("$<")'
data/hyfi-imp_03_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=3; MONTH="$*"; source("$<")'
data/hyfi-imp_04_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=4; MONTH="$*"; source("$<")'
data/hyfi-imp_05_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=5; MONTH="$*"; source("$<")'
data/hyfi-imp_06_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=6; MONTH="$*"; source("$<")'
data/hyfi-imp_07_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=7; MONTH="$*"; source("$<")'
data/hyfi-imp_08_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=8; MONTH="$*"; source("$<")'
data/hyfi-imp_09_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=9; MONTH="$*"; source("$<")'
data/hyfi-imp_10_%.RData : script.R/mi-build_df.R data/hyfi_%.RData data/mi-imputation-D_%.RData data/mi-imputation-H_%.RData R/util_219.R
	Rscript -e 'IMP=10; MONTH="$*"; source("$<")'

data/hyfi-ps-%.csv : data/hyfi-ps-%.RData
	touch $@

data/hyfi-ps-imp_01_%.RData : script.R/propensity-score.R data/hyfi-imp_01_%.RData
	Rscript -e 'IMP=1; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_01_$*.html
data/hyfi-ps-imp_02_%.RData : script.R/propensity-score.R data/hyfi-imp_02_%.RData
	Rscript -e 'IMP=2; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_02_$*.html
data/hyfi-ps-imp_03_%.RData : script.R/propensity-score.R data/hyfi-imp_03_%.RData
	Rscript -e 'IMP=3; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_03_$*.html
data/hyfi-ps-imp_04_%.RData : script.R/propensity-score.R data/hyfi-imp_04_%.RData
	Rscript -e 'IMP=4; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_04_$*.html
data/hyfi-ps-imp_05_%.RData : script.R/propensity-score.R data/hyfi-imp_05_%.RData
	Rscript -e 'IMP=5; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_05_$*.html
data/hyfi-ps-imp_06_%.RData : script.R/propensity-score.R data/hyfi-imp_06_%.RData
	Rscript -e 'IMP=6; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_06_$*.html
data/hyfi-ps-imp_07_%.RData : script.R/propensity-score.R data/hyfi-imp_07_%.RData
	Rscript -e 'IMP=7; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_07_$*.html
data/hyfi-ps-imp_08_%.RData : script.R/propensity-score.R data/hyfi-imp_08_%.RData
	Rscript -e 'IMP=8; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_08_$*.html
data/hyfi-ps-imp_09_%.RData : script.R/propensity-score.R data/hyfi-imp_09_%.RData
	Rscript -e 'IMP=9; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_09_$*.html
data/hyfi-ps-imp_10_%.RData : script.R/propensity-score.R data/hyfi-imp_10_%.RData
	Rscript -e 'IMP=10; MONTH="$*"; source("$<")' > www/hyfi-ps-imp_10_$*.html

##
www/cohort-monthly-descriptives.html : script.Rmd/cohort-monthly-descriptives.Rmd $(MONTHLY_RAW)
	Rscript -e 'OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'

www/cohort-monthly-entrances.html : script.Rmd/cohort-monthly-entrances.Rmd $(MONTHLY_RAW)
	Rscript -e 'OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'

www/cohort-monthly-descriptives-matched.html : script.Rmd/cohort-monthly-descriptives-matched.Rmd $(MATCHED_DATA)
	Rscript -e 'OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'

www/%_dintro-DD.html : www/_dintro-DD-TEMPLATE.html
	sed 's/DD/$*/g' $< > $@

www/%-imp_DD.html : www/_imp_DD-TEMPLATE.html
	sed 's/DD/$*/g' $< > $@

www/%-imp.dintro_DD.html : www/_imp-dintro_DD-TEMPLATE.html
	sed 's/DD/$*/g' $< > $@

data/hyfi-mtch-imp_%.csv : script.python/matching.py $(MONTHLY_PS)
	python $< $* $(MONTHS_LIST) > $@

data/hyfi-mtch-imp_%.RData : script.R/mtch-build_df.R data/hyfi-mtch-imp_%.csv $(MONTHLY_PS)
	Rscript -e 'IMP = $*; source("$<")'

www/cox-models-%-POOL.html : script.Rmd/cox-models-%-POOL.Rmd $(MATCHED_DATA)
	Rscript -e 'OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'

www/risk_comparision.html : script.Rmd/risk_comparision.Rmd $(MATCHED_DATA)
	Rscript -e 'OUT = ".tmp/$@/$(@F)"; IN = "$<"; source("$(RMD2HTML)")'
