if(!exists('MONTH')) MONTH = '2008-01'
if(!exists('IFILE')) IFILE = sprintf('data/mi-variable-selection_%s.RData', MONTH)
if(!exists('OFILE')) OFILE = sprintf('data/mi-imputation-%s_%s.RData', INTER, MONTH)

load(IFILE)

library(mice)

source('R/util_imputation.R')

### Matrius de predicció
mi_pred_matrix = lapply(res[[INTER]]$vars, function(l) union(l$na, l$value))
mi_pred_matrix = lapply(mi_pred_matrix, function(l) union(l, c('i.ep_aff.all', 'na_aff.all', 
                                                               'age', 'smoking', 'diabetes')))

imp = idiap_mi(D.inter[[INTER]], v_missing_order = NULL, mi_method = NULL, mi_pred_matrix=mi_pred_matrix, 
               seed = 1, m = 10, maxit=100)

save(imp, file = OFILE)