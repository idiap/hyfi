if(!exists('PROJ_ROOT')) PROJ_ROOT = "/home/idiap/projects.thor/hyfi"
library(dplyr)
library(survival)
library(dplyr)
library(ggplot2)
library(tidyr)
library(mice)

NIMP = 10
data = lapply(1:NIMP, function(IMP){
  load(sprintf("%s/data/hyfi-mtch-imp_%02d.RData", PROJ_ROOT, IMP))
  # Es copia l'mpr de l'usuari al control (controls i usuaris estan ordenats)
  data[,'mpr1y'] = rep(data[data$iuser == 1,'mpr1y'] %>% .[['mpr1y']], each=2)
  data[,'aff.event'] = with(data, rep(
    pmin(ifelse(i.ep_aff.all[data$iuser == 1] == 0, Inf, t.ep_aff.all[data$iuser == 1]),
         ifelse(i.ep_aff.all[data$iuser == 0] == 0, Inf, t.ep_aff.all[data$iuser == 0])), each = 2))
  data %>%
    select(ocip, age, dintro, mtch, user, iuser, mpr1y, risk.aff, ps, aff.event, 
           i.ep_aff.all, t.ep_aff.all) %>%
    mutate(imp = IMP)
}) %>% bind_rows

devtools::load_all('/home/idiap/lib/R/usrgi_lib')

eps = c('ep_aff.all')

mpr_period = function(days.treated, followup = rep(mpr.months * treat.duration.days, length(env)), 
                      treat.duration.days = 28, mpr.months = 12){
  res = pmin(followup, pmin(days.treated/treat.duration.days, mpr.months) * treat.duration.days) / pmin(followup, mpr.months * treat.duration.days)
  res[followup == 0] = 1
  res
}
d = bind_cols(
  data %>% dplyr::select(user, dintro, age, iuser, risk.aff, ps, imp, mtch, mpr1y, aff.event, one_of(sprintf('i.%s', eps))) %>% 
    gather(key=outcome, value=event, -user, -dintro, -age, -iuser, -risk.aff, -ps, -imp, -mtch, -mpr1y, -aff.event),
  data %>% dplyr::select(one_of(sprintf('t.%s', eps))) %>% 
    gather(key=outcome, value=time) %>%
    select(time)) %>% mutate(
      grisk_aff.4 = cut(100*risk.aff, breaks = c(0, 5, 10, 15, 100), include.lowest = T),
      grisk_aff.3a = cut(100*risk.aff, breaks = c(0, 5, 15, 100), include.lowest = T),
      grisk_aff.3b = cut(100*risk.aff, breaks = c(0, 5, 10, 100), include.lowest = T),
      mpr = 100*mpr_period(mpr1y, time),
      mpr80 = ifelse(mpr > 80, 'MPR1Y>80', 'MPR1Y≤80')
    )