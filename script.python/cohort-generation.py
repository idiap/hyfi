# -*- coding: utf-8 -*-
def html_pre(txt):
    return '<pre>' + txt + '</pre>'
def html_p(txt):
    return '<p>' + txt + '</p>'
def html_h3(txt):
    return '<h3>' + txt + '</h3>'
def html_h4(txt):
    return '<h4>' + txt + '</h4>'
def html_fig(txt):
    return '<img src="' + txt + '" />'

def count_by_years(dd):
  accum = 0
  txt = ""
  for f,c in sorted(dd.items(), reverse = True):
    accum = accum + c
    txt = txt + "%10s:%6d\t%6d" % (str(f),c,accum) + "\n"
  return txt

print '<html>'
print '<head>'
print '<meta charset="UTF-8">'
print '</head>'
print '<body>'

import sys
sys.path.insert(0, 'nova.local')

from nova.paths import Paths
PROJECT = 'hyfi'
paths = Paths(PROJECT, server='thor')
print html_pre("Reading data from : %s" % paths.get('.'))

from nova.population import Population
population = Population(filename = paths.get('poblacio.txt'), cmbd_path=paths.get('cmbd-ah.txt'))
print html_pre("219 (sidiapq) population : %d" % len(population))

# COHORT CONSTRUCTION
print html_h3("Cohort construction")

# Study period
import datetime
begin_study = datetime.date(2006,7,1)
end_study   = datetime.date(2013,12,31)

print html_pre("Study period: from %s to %s" % (begin_study, end_study))

intro_from = datetime.date(2006,7,1)
intro_until = datetime.date(2013,12,31)
print html_pre("Entrance: from %s to %s" % (intro_from, intro_until))

from nova.cohort import Cohort
cohort = Cohort(population = population,
                beginning = begin_study,
                ending = end_study,
                minIntro = intro_from,
                maxIntro = intro_until)
cohort.setStatusFilter(population)    # Alive, death or tranferred filter

print html_pre('People who died/translated before entrance: %d' %  (len(population) - len(cohort.cohort)))

###<<<<<< diagnostic of hypertension
print html_h4("Diagnostic of Hypertension")

from nova.problems import Problems
problems = Problems(paths, problem_definition = paths.get('hyfi-problems.xml'), cmbd_path=paths.get('cmbd-ah.txt'))

htn_not_org = problems.get_events(cohort.cohort, 'htn_not_org')
htn_not_org_ecap = { o: [d for d in htn_not_org['ecap'][o]] for o in set(htn_not_org['ecap'])}
htn_not_org_cmbd = { o: [d for d in htn_not_org['cmbd'][o]] for o in set(htn_not_org['cmbd'])}

print html_pre('HTN not-org. (ecap): %d' %  (len(htn_not_org_ecap)))
print html_pre('HTN not-org. (cmbd): %d' %  (len(htn_not_org_cmbd)))

dates = {o:[] for o in set(htn_not_org_ecap) | set(htn_not_org_cmbd)}
for o in htn_not_org_ecap:
    dates[o] = dates[o] + htn_not_org_ecap[o]
for o in htn_not_org_cmbd:
    dates[o] = dates[o] + htn_not_org_cmbd[o]

htn_pr = { o : min(dates[o]) for o in dates }
print html_pre('HTN not-org. (ecap+cmbd): %d' %  (len(htn_pr)))

import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from collections import Counter
counter = Counter([htn_pr[o].year for o in htn_pr])
cc = dict(counter)
x = [i[0] for i in cc.items()]
y = [i[1] for i in cc.items()]

fig, ax = plt.subplots()

ax.set_xlabel('Diagnostic (year)')
ax.set_ylabel('Counting')
ax.set_title('Hypertension diagnostic')
ax.stem(x, y, markerfmt=' ')

sub_axes = plt.axes([.2, .6, .45, .25], xticks=[1980, 1990, 2000, 2010, 2020], yticks=[])
sub_axes.stem(x, y, markerfmt=' ')
sub_axes.set_xlim([1980,2020])

fig.savefig('www/counting-htn.png')
print '<table>'
print '<tr>'
print '<td valign="top">'
print html_pre(count_by_years(cc))
print '</td>'
print '<td>                           </td>'
print '<td valign="top">'
print html_fig('counting-htn.png')
print '</td>'
print '</tr>'
print '</table>'
### diagnostic of hypertension >>>>>>>>>>>>>

###<<<<<< treatment of hypertension
print html_h4("Treament for Hypertension")

from nova.treatment import Treatment
from utils import misc
htn_facturation = ['c02', 'c03', 'c07', 'c08', 'c09']
htn_med = dict()
for med in htn_facturation:
    treatment = Treatment(population = population, filename = paths.get(med + '.txt'), catalog = paths.get('catalog'))
    treatment.iocip = 0
    treatment.iatc = 4
    treatment.ipfc = 5
    treatment.idate = 6
    treatment.ienv = 7
    ini_treat = treatment.firstFacturation(cohort.cohort)
    print html_pre('HTN %s treatment: %d' %  (med, len(ini_treat)))
    htn_med = misc.getMinDate(ini_treat, htn_med)

print html_pre('HTN treatment: %d' %  len(htn_med))

cc = dict(Counter([htn_med[o].year for o in htn_med]))
x = [i[0] for i in cc.items()]
y = [i[1] for i in cc.items()]

fig, ax = plt.subplots()

ax.set_xlabel('Facturation (year)')
ax.set_ylabel('Counting')
ax.set_title('Hypertension facturation')
ax.stem(x, y, markerfmt=' ')
ax.set_xlim([2004,2014])


fig.savefig('www/counting-htn_meds.png')
print '<table>'
print '<tr>'
print '<td valign="top">'
print html_pre(count_by_years(cc))
print '</td>'
print '<td>                           </td>'
print '<td valign="top">'
print html_fig('counting-htn_meds.png')
print '</td>'
print '</tr>'
print '</table>'
### treatment of hypertension >>>>>>>>>>>>>

print '<font color="red">' + html_p('%s people do not have neither diagnostic of hypertension nor treatment.' % ( len(cohort.cohort - (set(htn_pr) | set(htn_med))) )) + '</font>'

###<<< HTN summary
o_both = set(htn_pr) & set(htn_med)
o_pr = set(htn_pr) - set(htn_med)
o_med = set(htn_med) - set(htn_pr)

size =  1+(intro_until-intro_from).days

cum_both = numpy.zeros(shape=size)
cum_pr = numpy.zeros(shape=1+(intro_until-intro_from).days)
cum_med = numpy.zeros(shape=1+(intro_until-intro_from).days)
for o in o_both:
    cum_both[max(0, (htn_pr[o] - intro_from).days, (htn_med[o] - intro_from).days):size] += 1
    if htn_pr[o] < htn_med[o] and intro_from <= htn_med[o]:
        cum_pr[max(0, (htn_pr[o] - intro_from).days):(htn_med[o] - intro_from).days] += 1
    if htn_med[o] < htn_pr[o] and intro_from <= htn_pr[o]:
        cum_med[max(0, (htn_med[o] - intro_from).days):(htn_pr[o] - intro_from).days] += 1
for o in o_pr:
    cum_pr[max(0, (htn_pr[o] - intro_from).days):size] += 1
for o in o_med:
    cum_med[max(0, (htn_med[o] - intro_from).days):size] += 1

x = [intro_from + datetime.timedelta(days=x) for x in range(0, size)]
y_both = cum_both
y_pr = cum_both + cum_pr
y_med = cum_both + cum_pr + cum_med


fig, ax = plt.subplots()
ax.fill_between(x, 0, y_both, label='diagnostic and medication', facecolor='blue', alpha=0.5)
ax.fill_between(x, y_both, y_pr, label='only diagnostic', facecolor='green', alpha=0.5)
ax.fill_between(x, y_pr, y_med, label='only medication', facecolor='red', alpha=0.5)
ax.set_ylim([0,265000])

fig.savefig('www/entrances.png')
print html_fig('entrances.png')
### HTN summary >>>>

cohort.setMinIntroDate(misc.getMinDate(htn_pr, htn_med))

min_age = 55
max_age = 130
print "<ul><li>People not in (%d,%d) range are deleted</li></ul>" % (min_age, max_age)
cohort.setAgeFilter(population, min_age, max_age)

MEDEA = misc.getValue(paths.get('medea.txt'))
without_medea = cohort.cohort - set(MEDEA)
cohort.eliminate(without_medea)
print "<ul><li>People without MEDEA information: %d</li></ul>" % len(without_medea)

### Exclusion criterias

## Antecedents
l_antecedents = ['aff', 'dvt', 'oth_arr', 'ihd_atc', 'htn_org', 'stroke', 'tia', 'pad_atc']
d_antecedents = problems.get_events(cohort.cohort, l_antecedents)

dates = { o: [] for o in set(d_antecedents['ecap']) | set(d_antecedents['cmbd'])}
for o in d_antecedents['ecap']:
    dates[o] = dates[o] + list(d_antecedents['ecap'][o])
for o in d_antecedents['cmbd']:
    dates[o] = dates[o] + list(d_antecedents['cmbd'][o])

d_first_antecedents = { o : min(dates[o]) for o in dates }


## Procedures
l_procedures = ['219_proc']
procs = problems.get_procedures(cohort.cohort, l_procedures)
d_procedures = { o: min(list(procs[o])) for o in procs }

#### Medications
l_medication = ['c01-antecedent', 'b01aa', 'aspirine', 'surrogates']
d_medication = dict()
for med in l_medication:
    treatment = Treatment(population = population, filename = paths.get(med + '.txt'), catalog = paths.get('catalog'))
    d_medication = misc.getMinDate( treatment.firstFacturation(cohort.cohort), d_medication )

cohort.setMaxIntroDate(misc.addToDate(d_first_antecedents, datetime.timedelta(-1)))
cohort.setMaxIntroDate(misc.addToDate(d_procedures, datetime.timedelta(-1)))
cohort.setMaxIntroDate(misc.addToDate(d_medication, datetime.timedelta(-1)))

#### Eliminating prevalent user
statine_effect = 6
statines = Treatment(population = cohort.cohort, filename = paths.get('statine.txt'), catalog = paths.get('catalog') )
prevalent = statines.statusAt(cohort._minIntro, effect = datetime.timedelta(10000 * 30))
prev_user = set([ o for o in prevalent if prevalent[o] == 1 ])

cohort.eliminate(prev_user)
print "<ul><li>Prevalent users at entrance %d (considering %d months effect)</li></ul>" % (len(prev_user), 10000)

## Secondary elimination algorithm
l_incidences = set(['ami', 'stroke_i', 'stroke_h', 'tia', 'stroke_e', 'angor', 'ihd_acute', 'pad'])
ev_inc_all = { o : [cohort._exitus[o]] for o in cohort.cohort }
# For each cv event
for l_inc in l_incidences:
    #we look for events from cohort._minIntro
    ev_inc = problems.get_events(cohort.cohort, l_inc, dfrom=cohort._minIntro)
    ev_inc_only_ecap = { o: [d for d in ev_inc['ecap'][o]] for o in set(ev_inc['ecap']) - set(ev_inc['cmbd'])}
    ev_inc_only_cmbd = { o: [d for d in ev_inc['cmbd'][o]] for o in set(ev_inc['cmbd']) - set(ev_inc['ecap'])}
    ev_inc_in_both = { o: list(set([d for d in ev_inc['ecap'][o]]) | set([d for d in ev_inc['cmbd'][o]])) for o in set(ev_inc['cmbd']) & set(ev_inc['ecap'])}
    for o in ev_inc_only_ecap:
        ev_inc_all[o] = ev_inc_all[o] + ev_inc_only_ecap[o]
    for o in ev_inc_only_cmbd:
        ev_inc_all[o] = ev_inc_all[o] + ev_inc_only_cmbd[o]
    for o in ev_inc_in_both:
        ev_inc_all[o] = ev_inc_all[o] + ev_inc_in_both[o]
# We take either the minimum date between first event and exitus date
ev_inc_all = {o : min(ev_inc_all[o]) for o in ev_inc_all}

# We look for people with ihd_chronic before any CV avent
ihd_chronic = problems.get_events(cohort.cohort, 'ihd_chronic', dfrom=cohort._minIntro)
ihd_chronic_ecap = set( [o for o in ihd_chronic['ecap'] if min(ihd_chronic['ecap'][o]) + datetime.timedelta(30) < ev_inc_all[o]] )
ihd_chronic_cmbd = set( [o for o in ihd_chronic['cmbd'] if min(ihd_chronic['cmbd'][o]) + datetime.timedelta(30) < ev_inc_all[o]] )
o_ihd_chronic = ihd_chronic_ecap | ihd_chronic_cmbd

# We repeat the previous process using only information contained in the first 5th cmbd positions
ev_inc_5 = { o : [cohort._exitus[o]] for o in cohort.cohort }
for l_inc in l_incidences:
    ev_inc = problems.get_events(cohort.cohort, l_inc, dfrom=cohort._minIntro, var_diag = ['DP', 'DS1', 'DS2', 'DS3', 'DS4'])
    ev_inc_only_ecap = { o: [d for d in ev_inc['ecap'][o]] for o in set(ev_inc['ecap']) - set(ev_inc['cmbd'])}
    ev_inc_only_cmbd = { o: [d for d in ev_inc['cmbd'][o]] for o in set(ev_inc['cmbd']) - set(ev_inc['ecap'])}
    ev_inc_in_both = { o: list(set([d for d in ev_inc['ecap'][o]]) | set([d for d in ev_inc['cmbd'][o]])) for o in set(ev_inc['cmbd']) & set(ev_inc['ecap'])}
    for o in ev_inc_only_ecap:
        ev_inc_5[o] = ev_inc_5[o] + ev_inc_only_ecap[o]
    for o in ev_inc_only_cmbd:
        ev_inc_5[o] = ev_inc_5[o] + ev_inc_only_cmbd[o]
    for o in ev_inc_in_both:
        ev_inc_5[o] = ev_inc_5[o] + ev_inc_in_both[o]
ev_inc_5 = {o : min(ev_inc_5[o]) for o in ev_inc_5}
# Those people with ev_inc_all[o] < ev_inc_5[o] are those with registry in positions 6th and so on without registry in positions 1 to 4.
o_cmbd_position = set([o for o in ev_inc_5 if ev_inc_all[o] < ev_inc_5[o] ])

print "<ul><li>Secondary algorithm: ihd_chronic (%d), fifth position (%d)</li></ul>" % (len(o_ihd_chronic), len(o_cmbd_position))
print "<ul><li>People eliminated by the secondary algorithm: %d</li></ul>" % len(o_cmbd_position | o_ihd_chronic)

cohort.eliminate(o_ihd_chronic | o_cmbd_position)

### We save the information for the current cohort
ofile = file('data/population_entrance_period.csv', 'w')
ofile.write(';'.join(['ocip', 'minIntro', 'maxIntro', 'dexitus', 'exitus']) + '\n')
for o in list(cohort.cohort):
    ofile.write(';'.join([o, str(cohort._minIntro[o]), str(cohort._maxIntro[o]), str(cohort._exitus[o]), cohort._exitus_reason[o]]) + '\n')
ofile.close()


#### Statine users
M = 1
hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia']

ev = problems.get_events(cohort.cohort, hard_ep)
dates = dict(ev['ecap'].items())
hard_ev = { o : min(dates[o]) for o in dates }
max_beginning  = misc.getMinDate(cohort._maxIntro, misc.addToDate(hard_ev, datetime.timedelta( -30 * M )))

beginning_all = statines.firstBeginning(cohort.cohort, months = statine_effect,
                                        dfrom = cohort._minIntro, duntil = max_beginning, getAll=True, ordered = False)
beginning = {o: beginning_all[o][0] for o in beginning_all}
ending = statines.firstEnding(beginning, months = statine_effect,
                              dfrom = misc.addToDate(beginning, datetime.timedelta(-1)), ordered = False)

user_cohort = Cohort(population = population,
                     beginning = begin_study,
                     ending = end_study,
                     minIntro = intro_from,
                     maxIntro = intro_until)
user_cohort.setStatusFilter(population)    # Alive, death or tranferred filter
user_cohort.eliminate(user_cohort.cohort - set(beginning_all))

cohort.setMaxIntroDate(beginning)

birth = {o: '%4d' % population[o][0].year for o in cohort.cohort}
#sex = {o: population[o][1] for o in cohort.cohort}

# sampling_sets = {k1 + k2: [] for k1 in set(birth.values()) for k2 in set(sex.values())}
# for o in cohort.cohort - set(beginning):
#     sampling_sets[birth[o]+sex[o]].append(o)
sampling_sets = {k1: [] for k1 in set(birth.values())}
for o in cohort.cohort - set(beginning):
    sampling_sets[birth[o]].append(o)

numpy.random.seed(1)

din_cohort = {d: {} for d in set(beginning.values())}
for o in beginning:
    date = beginning[o]
    #bag = [o1 for o1 in sampling_sets[birth[o]+sex[o]] if cohort._minIntro[o1] <= date and date <= cohort._maxIntro[o1] ]
    bag = [o1 for o1 in sampling_sets[birth[o]] if cohort._minIntro[o1] <= date and date <= cohort._maxIntro[o1] ]
    din_cohort[date][o] = list(numpy.random.choice(bag, min(100, len(bag)), replace = False))

ofile = open('data/cohort-generation.csv', 'w')
for d in din_cohort:
    ofile_month = open('data/cohort-generation-%s.csv' % str(d)[0:7], 'w')
    for o in din_cohort[d]:
        line = [str(d), o] + din_cohort[d][o]
        ofile.write(';'.join(line) + '\n')
        ofile_month.write(';'.join(line) + '\n')
    ofile_month.close()
ofile.close()


print '</body>'
print '</html>'
#####
