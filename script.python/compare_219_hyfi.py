import sys
sys.path.insert(0, 'nova.local')

from nova.paths import Paths

paths_hyfi = Paths('hyfi', server='thor')
paths_219 = Paths('2012', server='thor')


from nova.population import Population
pop_hyfi = Population(filename = paths_hyfi.get('poblacio.txt'), 
                      cmbd_path=paths_hyfi.get('cmbd-ah.txt'))
#In [27]: len(pop_hyfi)
#Out[27]: 263530

pop_219 = Population(filename = paths_219.get('population/population-sidiapq-dates_sense_barres.txt'), 
                     cmbd_path=paths_219.get('cmbd/cmbd.txt'), istatus_cmbd = 15, idstatus_cmbd = 14)
#In [28]: len(pop_219)
#Out[28]: 2086136

import datetime
begin_study = datetime.date(2006,7,1)
end_study   = datetime.date(2011,12,31)

intro_from = datetime.date(2006,7,1)
intro_until = datetime.date(2006,7,1)

from nova.cohort import Cohort
cohort_hyfi = Cohort(population = pop_hyfi,
                     beginning = begin_study,
                     ending = end_study,
                     minIntro = intro_from,
                     maxIntro = intro_until)

cohort_219 = Cohort(population = pop_219,
                    beginning = begin_study,
                    ending = end_study,
                    minIntro = intro_from,
                    maxIntro = intro_until)

cohort_hyfi.setStatusFilter(pop_hyfi)    # Alive, death or tranferred filter
#In [4]: len(cohort_hyfi.cohort)
#Out[4]: 263509

cohort_219.setStatusFilter(pop_219)    # Alive, death or tranferred filter
#In [6]: len(cohort_219.cohort)
#Out[6]: 2022426

cohort_hyfi.setAgeFilter(pop_hyfi, minAge = 55, maxAge = 130)
#In [11]: len(cohort_hyfi.cohort)
#Out[11]: 223389

cohort_219.setAgeFilter(pop_219, minAge = 55, maxAge = 130)
#In [13]: len(cohort_219.cohort)
#Out[13]: 538666


from nova.problems import Problems
probs_hyfi = Problems(paths_hyfi, problem_definition = paths_hyfi.get('hyfi-problems.xml'), cmbd_path=paths_hyfi.get('cmbd-ah.txt'))

probs_219 = Problems(paths_219, cmbd_path = paths_219.get('cmbd/cmbd.txt'), 
                     ecap_path = paths_219.get('problems/problems.txt'), 
                     problem_definition = '/home/idiap/data.conan/eprevf/eprevf-problems.xml')
probs_219.cmbd_idate = 12
probs_219.cmbd_diag = {'DP' : 20, 'DS1': 21, 'DS2' : 22, 'DS3' : 23, 'DS4' : 24, 'DS5' : 25, 'DS6' : 26, 'DS7' : 27, 'DS8' : 28, 'DS9' : 29}
probs_219.cmbd_proc = {'PP' : 35, 'PS1': 36, 'PS2' : 37, 'PS3' : 38, 'PS4' : 39, 'PS5' : 40, 'PS6' : 41, 'PS7' : 42}

def get_first_event(probs, cohort):
	htn_not_org = probs.get_events(cohort.cohort, 'htn_not_org')
	htn_not_org_ecap = { o: [d for d in htn_not_org['ecap'][o]] for o in set(htn_not_org['ecap'])}
	htn_not_org_cmbd = { o: [d for d in htn_not_org['cmbd'][o]] for o in set(htn_not_org['cmbd'])}
	dates = {o:[] for o in set(htn_not_org_ecap) | set(htn_not_org_cmbd)}
	for o in htn_not_org_ecap:
		dates[o] = dates[o] + htn_not_org_ecap[o]
	for o in htn_not_org_cmbd:
		dates[o] = dates[o] + htn_not_org_cmbd[o]
	return { o : min(dates[o]) for o in dates }

## Only events until 2011 are considered
htn_not_org_hyfi = get_first_event(probs_hyfi, cohort_hyfi)
htn_not_org_hyfi = {o : htn_not_org_hyfi[o] for o in htn_not_org_hyfi if htn_not_org_hyfi[o].year <= 2011}

htn_not_org_219 = get_first_event(probs_219, cohort_219)
htn_not_org_219 = {o : htn_not_org_219[o] for o in htn_not_org_219 if htn_not_org_219[o].year <= 2011}

from nova.treatment import Treatment
from utils import misc
htn_facturation = ['c02', 'c03', 'c07', 'c08', 'c09']

def get_first_treatment(population, cohort, treatments, paths, root, iocip = 0, idate = 6, iatc = 4, ipfc = 5, ienv = 7):
	htn_med = dict()
	for med in treatments:
		treatment = Treatment(population = population, filename = paths.get(root + med + '.txt'), catalog = paths.get('catalog'))
		treatment.iocip = iocip
		treatment.idate = idate #1
		treatment.iatc = iatc #2
		treatment.ipfc = ipfc #3
		treatment.ienv = ienv #4
		ini_treat = treatment.firstFacturation(cohort.cohort)
		htn_med = misc.getMinDate(ini_treat, htn_med)
	return htn_med


htn_med_hyfi = get_first_treatment(pop_hyfi, cohort_hyfi, htn_facturation, paths_hyfi, '')
htn_med_hyfi = {o :htn_med_hyfi[o] for o in htn_med_hyfi if htn_med_hyfi[o].year <= 2011}

htn_med_219 = get_first_treatment(pop_219, cohort_219, htn_facturation, paths_219, 'facturation/facturation-data/', 
                                  idate = 1, iatc = 2, ipfc = 3, ienv = 4)
htn_med_219 = {o :htn_med_219[o] for o in htn_med_219 if htn_med_219[o].year <= 2011}


cohort_hyfi.setMinIntroDate(misc.getMinDate(htn_not_org_hyfi, htn_med_hyfi))
#In [18]: len(cohort_hyfi.cohort)
#Out[18]: 162212

cohort_219.setMinIntroDate(misc.getMinDate(htn_not_org_219, htn_med_219))
#In [19]: len(cohort_219.cohort)
#Out[19]: 442362

l_antecedents = ['aff', 'dvt', 'oth_arr', 'ihd_atc', 'htn_org', 'stroke', 'tia', 'pad_atc']

def get_first_event_list(problems, cohort, l_antecedents):
	d_antecedents = problems.get_events(cohort.cohort, l_antecedents)

	dates = { o: [] for o in set(d_antecedents['ecap']) | set(d_antecedents['cmbd'])}
	for o in d_antecedents['ecap']:
		dates[o] = dates[o] + list(d_antecedents['ecap'][o])
	for o in d_antecedents['cmbd']:
		dates[o] = dates[o] + list(d_antecedents['cmbd'][o])

	return { o : min(dates[o]) for o in dates }

d_first_antecedents_hyfi = get_first_event_list(probs_hyfi, cohort_hyfi, l_antecedents)
d_first_antecedents_219 = get_first_event_list(probs_219, cohort_219, l_antecedents)

cohort_hyfi.setMaxIntroDate(misc.addToDate(d_first_antecedents_hyfi, datetime.timedelta(-1)))
#In [22]: len(cohort_hyfi.cohort)
#Out[22]: 1161985

cohort_219.setMaxIntroDate(misc.addToDate(d_first_antecedents_219, datetime.timedelta(-1)))
#In [23]:  len(cohort_219.cohort)
#Out[23]: 363442

l_procedures = ['219_proc']
def get_first_procedure_list(problems, cohort, l_procedures):
	procs = problems.get_procedures(cohort.cohort, l_procedures)
	return { o: min(list(procs[o])) for o in procs }

d_procedures_hyfi = get_first_procedure_list(probs_hyfi, cohort_hyfi, l_procedures)
d_procedures_219 = get_first_procedure_list(probs_219, cohort_219, l_procedures)

cohort_hyfi.setMaxIntroDate(misc.addToDate(d_procedures_hyfi, datetime.timedelta(-1)))
#In [25]: len(cohort_hyfi.cohort)
#Out[25]: 161983

cohort_219.setMaxIntroDate(misc.addToDate(d_procedures_219, datetime.timedelta(-1)))
#In [26]: len(cohort_219.cohort)
#Out[26]: 363415

l_medication = ['c01-antecedent', 'b01aa', 'aspirine', 'surrogates']
d_med_hyfi = get_first_treatment(pop_hyfi, cohort_hyfi, l_medication, paths_hyfi, '')
d_med_219 = get_first_treatment(pop_219, cohort_219, l_medication, paths_219, 'facturation/facturation-compound/', 
                                idate = 1, iatc = 2, ipfc = 3, ienv = 4)

cohort_hyfi.setMaxIntroDate(misc.addToDate(d_med_hyfi, datetime.timedelta(-1)))
#In [25]: len(cohort_hyfi.cohort)
#Out[25]: 157195

cohort_219.setMaxIntroDate(misc.addToDate(d_med_219, datetime.timedelta(-1)))
#In [26]: len(cohort_219.cohort)
#Out[26]: 297907

MEDEA_hyfi = misc.getValue(paths_hyfi.get('medea.txt'))
without_medea = cohort_hyfi.cohort - set(MEDEA_hyfi)
cohort_hyfi.eliminate(without_medea)
#In [50]: len(cohort_hyfi.cohort)
#Out[50]: 154902

MEDEA_219 = misc.getValue(paths_219.get('medea/medea.txt'))
without_medea = cohort_219.cohort - set([o for o in MEDEA_219 if MEDEA_219[o] != 'NA'])
cohort_219.eliminate(without_medea)
#In [51]: len(cohort_219.cohort)
#Out[51]: 274525





## Secondary elimination algorithm
def secondary_elimination(cohort, problems):
	l_incidences = set(['ami', 'stroke_i', 'stroke_h', 'tia', 'stroke_e', 'angor', 'ihd_acute', 'pad'])
	ev_inc_all = { o : [cohort._exitus[o]] for o in cohort.cohort }
	# For each cv event
	for l_inc in l_incidences:
		  #we look for events from cohort._minIntro
		  ev_inc = problems.get_events(cohort.cohort, l_inc, dfrom=cohort._minIntro)
		  ev_inc_only_ecap = { o: [d for d in ev_inc['ecap'][o]] for o in set(ev_inc['ecap']) - set(ev_inc['cmbd'])}
		  ev_inc_only_cmbd = { o: [d for d in ev_inc['cmbd'][o]] for o in set(ev_inc['cmbd']) - set(ev_inc['ecap'])}
		  ev_inc_in_both = { o: list(set([d for d in ev_inc['ecap'][o]]) | set([d for d in ev_inc['cmbd'][o]])) for o in set(ev_inc['cmbd']) & set(ev_inc['ecap'])}
		  for o in ev_inc_only_ecap:
		      ev_inc_all[o] = ev_inc_all[o] + ev_inc_only_ecap[o]
		  for o in ev_inc_only_cmbd:
		      ev_inc_all[o] = ev_inc_all[o] + ev_inc_only_cmbd[o]
		  for o in ev_inc_in_both:
		      ev_inc_all[o] = ev_inc_all[o] + ev_inc_in_both[o]
	# We take either the minimum date between first event and exitus date
	ev_inc_all = {o : min(ev_inc_all[o]) for o in ev_inc_all}

	# We look for people with ihd_chronic before any CV avent
	ihd_chronic = problems.get_events(cohort.cohort, 'ihd_chronic', dfrom=cohort._minIntro)
	ihd_chronic_ecap = set( [o for o in ihd_chronic['ecap'] if min(ihd_chronic['ecap'][o]) + datetime.timedelta(30) < ev_inc_all[o]] )
	ihd_chronic_cmbd = set( [o for o in ihd_chronic['cmbd'] if min(ihd_chronic['cmbd'][o]) + datetime.timedelta(30) < ev_inc_all[o]] )
	o_ihd_chronic = ihd_chronic_ecap | ihd_chronic_cmbd

	# We repeat the previous process using only information contained in the first 5th cmbd positions
	ev_inc_5 = { o : [cohort._exitus[o]] for o in cohort.cohort }
	for l_inc in l_incidences:
		  ev_inc = problems.get_events(cohort.cohort, l_inc, dfrom=cohort._minIntro, var_diag = ['DP', 'DS1', 'DS2', 'DS3', 'DS4'])
		  ev_inc_only_ecap = { o: [d for d in ev_inc['ecap'][o]] for o in set(ev_inc['ecap']) - set(ev_inc['cmbd'])}
		  ev_inc_only_cmbd = { o: [d for d in ev_inc['cmbd'][o]] for o in set(ev_inc['cmbd']) - set(ev_inc['ecap'])}
		  ev_inc_in_both = { o: list(set([d for d in ev_inc['ecap'][o]]) | set([d for d in ev_inc['cmbd'][o]])) for o in set(ev_inc['cmbd']) & set(ev_inc['ecap'])}
		  for o in ev_inc_only_ecap:
		      ev_inc_5[o] = ev_inc_5[o] + ev_inc_only_ecap[o]
		  for o in ev_inc_only_cmbd:
		      ev_inc_5[o] = ev_inc_5[o] + ev_inc_only_cmbd[o]
		  for o in ev_inc_in_both:
		      ev_inc_5[o] = ev_inc_5[o] + ev_inc_in_both[o]
	ev_inc_5 = {o : min(ev_inc_5[o]) for o in ev_inc_5}
	# Those people with ev_inc_all[o] < ev_inc_5[o] are those with registry in positions 6th and so on without registry in positions 1 to 4.
	o_cmbd_position = set([o for o in ev_inc_5 if ev_inc_all[o] < ev_inc_5[o] ])

	cohort.eliminate(o_ihd_chronic | o_cmbd_position)

secondary_elimination(cohort_hyfi, probs_hyfi)
#In [35]: len(cohort_hyfi.cohort)
#Out[35]: 160079

secondary_elimination(cohort_219, probs_219)
#In [37]: len(cohort_219.cohort)
#Out[37]: 303692




cohort_hyfi.setIntro()
cohort_219.setIntro()

cohort_hyfi.addData(cohort_hyfi.cohortIntro, 'dintro')
cohort_hyfi.addData(pop_hyfi.sex(cohort_hyfi.cohort), 'sex')
cohort_hyfi.addData(pop_hyfi.ageAt(cohort_hyfi.cohortIntro), 'age')

cohort_219.addData(cohort_219.cohortIntro, 'dintro')
cohort_219.addData(pop_219.sex(cohort_219.cohort), 'sex')
cohort_219.addData(pop_219.ageAt(cohort_219.cohortIntro), 'age')

cohort_hyfi.addData(cohort_hyfi._exitus_reason, 'exitus')
cohort_hyfi.addData(cohort_hyfi._exitus, 'dexitus')

cohort_219.addData(cohort_219._exitus_reason, 'exitus')
cohort_219.addData(cohort_219._exitus, 'dexitus')

l_incidences = ['aff']
def add_incidence(problems, cohort, l_incidences):
	for l_inc in l_incidences:
		ev_inc = problems.get_events(cohort.cohort, l_inc, dfrom=cohort.cohortIntro, duntil=cohort._exitus)
		ev_inc_ecap = { o: [d for d in ev_inc['ecap'][o]] for o in set(ev_inc['ecap']) - set(ev_inc['cmbd'])}
		ev_inc_cmbd = { o: [d for d in ev_inc['cmbd'][o]] for o in set(ev_inc['cmbd']) - set(ev_inc['ecap'])}
		ev_inc_both = { o: list(set([d for d in ev_inc['ecap'][o]]) | set([d for d in ev_inc['cmbd'][o]])) for o in set(ev_inc['cmbd']) & set(ev_inc['ecap'])}
		cohort.addData({o : min(ev_inc_ecap[o]) for o in ev_inc_ecap}, 'ep_' + l_inc +'.ecap')
		cohort.addData({o : min(ev_inc_cmbd[o]) for o in ev_inc_cmbd}, 'ep_' + l_inc +'.cmbd')
		cohort.addData({o : min(ev_inc_both[o]) for o in ev_inc_both}, 'ep_' + l_inc +'.both')

add_incidence(probs_hyfi, cohort_hyfi, l_incidences)
add_incidence(probs_219, cohort_219, l_incidences)


cohort_hyfi.writeTable('data/hyfi_ocips.txt')
cohort_219.writeTable('data/219_ocips.txt')

#ofile = open('data/hyfi_ocips.txt', 'w')
#for o in cohort_hyfi.cohort:
#	ofile.write(o + '\n')
#ofile.close()

#ofile = open('data/219_ocips.txt', 'w')
#for o in cohort_219.cohort:
#	ofile.write(o + '\n')
#ofile.close()


