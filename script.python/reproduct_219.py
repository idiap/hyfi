import sys
sys.path.insert(0, 'nova.local')

from nova.paths import Paths
paths_219 = Paths('2012', server='thor')

from nova.population import Population
pop_219 = Population(filename = paths_219.get('population/population-sidiapq-dates_sense_barres.txt'), 
                     cmbd_path=paths_219.get('cmbd/cmbd.txt'), istatus_cmbd = 15, idstatus_cmbd = 14)

from nova.problems import Problems

probs_219 = Problems(paths_219, cmbd_path = paths_219.get('cmbd/cmbd.txt'), 
                     ecap_path = paths_219.get('problems/problems.txt'), 
                     problem_definition = '/home/idiap/data.thor/hyfi/hyfi-problems.xml')
probs_219.cmbd_idate = 12
probs_219.cmbd_diag = {'DP' : 20, 'DS1': 21, 'DS2' : 22, 'DS3' : 23, 'DS4' : 24, 'DS5' : 25, 'DS6' : 26, 'DS7' : 27, 'DS8' : 28, 'DS9' : 29}
probs_219.cmbd_proc = {'PP' : 35, 'PS1': 36, 'PS2' : 37, 'PS3' : 38, 'PS4' : 39, 'PS5' : 40, 'PS6' : 41, 'PS7' : 42}

from nova.treatment import Treatment
from utils import misc

def get_first_htn_not_org(probs, population):
	htn_not_org = probs.get_events(population, 'htn_not_org')
	htn_not_org_ecap = { o: [d for d in htn_not_org['ecap'][o]] for o in set(htn_not_org['ecap'])}
	htn_not_org_cmbd = { o: [d for d in htn_not_org['cmbd'][o]] for o in set(htn_not_org['cmbd'])}
	dates = {o:[] for o in set(htn_not_org_ecap) | set(htn_not_org_cmbd)}
	for o in htn_not_org_ecap:
		dates[o] = dates[o] + htn_not_org_ecap[o]
	for o in htn_not_org_cmbd:
		dates[o] = dates[o] + htn_not_org_cmbd[o]
	return { o : min(dates[o]) for o in dates }

def get_first_treatment(population, treatments, paths, root, iocip = 0, idate = 6, iatc = 4, ipfc = 5, ienv = 7):
	htn_med = dict()
	for med in treatments:
		treatment = Treatment(population = population, filename = paths.get(root + med + '.txt'), catalog = paths.get('catalog'))
		treatment.iocip = iocip
		treatment.idate = idate #1
		treatment.iatc = iatc #2
		treatment.ipfc = ipfc #3
		treatment.ienv = ienv #4
		ini_treat = treatment.firstFacturation(population)
		htn_med = misc.getMinDate(ini_treat, htn_med)
	return htn_med

htn_not_org = get_first_htn_not_org(probs_219, pop_219)
htn_not_org = {o : htn_not_org[o] for o in htn_not_org if htn_not_org[o].year <= 2011}

htn_facturation = ['c02', 'c03', 'c07', 'c08', 'c09']
htn_med_219 = get_first_treatment(pop_219, htn_facturation, paths_219, 'facturation/facturation-data/', 
                                  idate = 1, iatc = 2, ipfc = 3, ienv = 4)
htn_med_219 = {o :htn_med_219[o] for o in htn_med_219 if htn_med_219[o].year <= 2011}

htn = misc.getMinDate(htn_not_org, htn_med_219)


pop_219 = Population(filename = paths_219.get('population/population-sidiapq-dates_sense_barres.txt'), filtering = htn,
                     cmbd_path=paths_219.get('cmbd/cmbd.txt'), istatus_cmbd = 15, idstatus_cmbd = 14)

import datetime
begin_study = datetime.date(2006,7,1)
end_study   = datetime.date(2011,12,31)

intro_from = datetime.date(2006,7,1)
intro_until = datetime.date(2011,12,31)

from nova.cohort import Cohort
cohort_219 = Cohort(population = pop_219,
                    beginning = begin_study,
                    ending = end_study,
                    minIntro = intro_from,
                    maxIntro = intro_until)

cohort_219.setAgeFilter(pop_219, minAge = 55, maxAge = 130)
#In [16]: len(cohort_219.cohort)
#Out[16]: 472515

cohort_219.setStatusFilter(pop_219)    # Alive, death or tranferred filter
#In [18]: len(cohort_219.cohort)
#Out[18]: 456892

cohort_219.setMinIntroDate(htn)
#In [22]: len(cohort_219.cohort)
#Out[22]: 456378


l_antecedents = ['aff', 'dvt', 'oth_arr', 'ihd_atc', 'htn_org', 'stroke', 'tia', 'pad_atc']

def get_first_event_list(problems, cohort, l_antecedents):
	d_antecedents = problems.get_events(cohort.cohort, l_antecedents)

	dates = { o: [] for o in set(d_antecedents['ecap']) | set(d_antecedents['cmbd'])}
	for o in d_antecedents['ecap']:
		dates[o] = dates[o] + list(d_antecedents['ecap'][o])
	for o in d_antecedents['cmbd']:
		dates[o] = dates[o] + list(d_antecedents['cmbd'][o])

	return { o : min(dates[o]) for o in dates }

d_first_antecedents_219 = get_first_event_list(probs_219, cohort_219, l_antecedents)
cohort_219.setMaxIntroDate(misc.addToDate(d_first_antecedents_219, datetime.timedelta(-1)))
#In [21]: len(cohort_219.cohort)
#Out[21]: 359799

l_procedures = ['219_proc']
def get_first_procedure_list(problems, cohort, l_procedures):
	procs = problems.get_procedures(cohort.cohort, l_procedures)
	return { o: min(list(procs[o])) for o in procs }

d_procedures_219 = get_first_procedure_list(probs_219, cohort_219, l_procedures)

l_medication = ['c01-antecedent', 'b01aa', 'aspirine']
d_med_219 = get_first_treatment(pop_219, l_medication, paths_219, 'facturation/facturation-compound/', idate = 1, iatc = 2, ipfc = 3, ienv = 4)

cohort_219.setMaxIntroDate(misc.addToDate(d_med_219, datetime.timedelta(-1)))
#In [33]: len(cohort_219.cohort)
#Out[33]: 285075








