import csv
import sys

IMP = sys.argv[1]
MONTHS = sys.argv[2] # MONTHS = "2006-07:2006-08:2006-09:2006-10:2006-11:2006-12:2007-01:2007-02:2007-03:2007-04:2007-05:2007-06:2007-07:2007-08:2007-09:2007-10:2007-11:2007-12:2008-01:2008-02:2008-03:2008-04:2008-05:2008-06:2008-07:2008-08:2008-09:2008-10:2008-11:2008-12:2009-01:2009-02:2009-03:2009-04:2009-05:2009-06:2009-07:2009-08:2009-09:2009-10:2009-11:2009-12:2010-01:2010-02:2010-03:2010-04:2010-05:2010-06:2010-07:2010-08:2010-09:2010-10:2010-11:2010-12:2011-01:2011-02:2011-03:2011-04:2011-05:2011-06:2011-07:2011-08:2011-09:2011-10:2011-11:2011-12:2012-01:2012-02:2012-03:2012-04:2012-05:2012-06:2012-07:2012-08:2012-09:2012-10:2012-11:2012-12:2013-01:2013-02:2013-03:2013-04:2013-05:2013-06"

ind_match = 0
for month in MONTHS.split(':'):
    fname = 'data/hyfi-ps-imp_%s_%s.csv' % (IMP, month)
    ps = dict()
    ifile = open(fname, 'r')
    reader = csv.reader(ifile, delimiter = ';')
    for row in reader:
        ps[row[0]] = float(row[2])
    ifile.close()
    fname = 'data/cohort-generation-%s.csv' % month
    ifile = open(fname, 'r')
    reader = csv.reader(ifile, delimiter = ';')
    for row in reader:
        ind_match = ind_match + 1
        ps_user = ps[row[1]]
        diff = 100000
        for i in range(2,len(row)):
            n_diff = abs(ps[row[i]]-ps_user)
            if n_diff < diff:
                diff = n_diff
                i_control = i
        print row[1] + ';' + month + ';1;' + str(ind_match) + ';' + str(diff)
        print row[i_control] + ';' + month + ';0;' + str(ind_match) + ';' + str(diff)
    ifile.close()
