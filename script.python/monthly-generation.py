# -*- coding: utf-8 -*-
def html_pre(txt):
    return '<pre>' + txt + '</pre>'
def html_p(txt):
    return '<p>' + txt + '</p>'
def html_h3(txt):
    return '<h3>' + txt + '</h3>'
def html_h4(txt):
    return '<h4>' + txt + '</h4>'
def html_fig(txt):
    return '<img src="' + txt + '" />'

def count_by_years(dd):
  accum = 0
  txt = ""
  for f,c in sorted(dd.items(), reverse = True):
    accum = accum + c
    txt = txt + "%10s:%6d\t%6d" % (str(f),c,accum) + "\n"
  return txt

print '<html>'
print '<head>'
print '<meta charset="UTF-8">'
print '</head>'
print '<body>'

import sys
DATE = sys.argv[1]
# DATE = '2007-01'

import csv
ifile = open('data/cohort-generation-%s.csv' % DATE, 'r')
reader = csv.reader(ifile, delimiter=';')
month_population = set()
for row in reader:
    month_population = month_population | set(row[1:])
ifile.close()


sys.path.insert(0, 'nova.local')

from nova.paths import Paths
PROJECT = 'hyfi'
paths = Paths(PROJECT, server='thor')
print html_pre("Reading data from : %s" % paths.get('.'))

from nova.population import Population
population = Population(filename = paths.get('poblacio.txt'), filtering=month_population, cmbd_path=paths.get('cmbd-ah.txt'))
print html_pre("219 (sidiapq) population : %d" % len(population))

# COHORT CONSTRUCTION
print html_h3("Cohort construction")

# Study period
import datetime
begin_study = datetime.date(2006,7,1)
end_study   = datetime.date(2013,12,31)

print html_pre("Study period: from %s to %s" % (begin_study, end_study))

intro_from = datetime.date(2006,7,1)
intro_until = datetime.date(2013,12,31)
print html_pre("Entrance: from %s to %s" % (intro_from, intro_until))

from nova.cohort import Cohort
cohort = Cohort(population = population,
                beginning = begin_study,
                ending = end_study,
                minIntro = intro_from,
                maxIntro = intro_until)
cohort.setStatusFilter(population)    # Alive, death or tranferred filter
cohort.setIntro(datetime.date(int(DATE[0:4]), int(DATE[5:7]), 28))

from nova.treatment import Treatment

surrogates = Treatment(population = cohort.cohort, filename = paths.get('surrogates.txt') )
cohort.addData(surrogates.firstFacturation(cohort.cohort), 'dsurrogate')

statine_effect = 6
statines = Treatment(population = cohort.cohort, filename = paths.get('statine.txt'), catalog = paths.get('catalog') )
statines.filtering(population)
#### Statine users
from utils import misc

M = 1
hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia']

from nova.problems import Problems
problems = Problems(paths, problem_definition = paths.get('hyfi-problems.xml'), cmbd_path=paths.get('cmbd-ah.txt'))

ev = problems.get_events(cohort.cohort, hard_ep)
dates = dict(ev['ecap'].items())
hard_ev = { o : min(dates[o]) for o in dates }
max_beginning  = misc.getMinDate(cohort.cohortIntro, misc.addToDate(hard_ev, datetime.timedelta( -30 * M )))

beginning_all = statines.firstBeginning(cohort.cohort, months = statine_effect,
                                        dfrom = cohort.cohortIntro, duntil = max_beginning, getAll=True, ordered = False)
beginning = {o: beginning_all[o][0] for o in beginning_all}
ending = statines.firstEnding(beginning, months = statine_effect,
                              dfrom = misc.addToDate(beginning, datetime.timedelta(-1)), ordered = False)

global_beginning_all = statines.firstBeginning(cohort.cohort, months = statine_effect,
                                               dfrom = cohort.cohortIntro, duntil = cohort._exitus, getAll=True, ordered = False)
global_beginning = {o: global_beginning_all[o][0] for o in global_beginning_all}
global_ending = statines.firstEnding(global_beginning, months = statine_effect,
                                     dfrom = misc.addToDate(global_beginning, datetime.timedelta(-1)), ordered = False)

cohort.addData(cohort.cohortIntro, 'dintro')
cohort.addData(population.ageAt(cohort.cohortIntro), 'age')
cohort.addData(population.sex(cohort.cohort), 'sex')

cohort.addBinaryData(beginning, 'user')
cohort.addData(beginning_all, ['stat.beg', 'stat.atc', 'stat.env', 'stat.comp', 'stat.dose'])
cohort.addData(ending, 'stat.end')

cohort.addData(global_beginning_all, ['stat.beg.all', 'stat.atc.all', 'stat.env.all', 'stat.comp.all', 'stat.dose.all'])
cohort.addData(global_ending, 'stat.end.all')

mpr = statines.mpr(beginning, dfrom=beginning, duntil=ending)
mpr6m = statines.mpr(beginning, dfrom=beginning, duntil=misc.addToDate(beginning, datetime.timedelta(6*30)))
mpr1y = statines.mpr(beginning, dfrom=beginning, duntil=misc.addToDate(beginning, datetime.timedelta(12*30)))

cohort.addData(mpr, 'mpr')
cohort.addData(mpr6m, 'mpr6m')
cohort.addData(mpr1y, 'mpr1y')

cohort.addData(misc.getValue(paths.get('medea.txt')), 'medea')
up = population.up(cohort.cohort)
uba = population.uba(cohort.cohort)
cohort.addData(up, 'up')
cohort.addData(uba, 'uba')

ifile = open(paths.get('uba_vars.txt'), 'r')
reader = csv.reader(ifile, delimiter='@')
l_up_uba = dict()
for row in reader:
    key = (row[1], row[2])
    if key not in l_up_uba:
        l_up_uba[key] = []
    l_up_uba[key].append( (int(row[0]), [row[3], row[4], row[5], row[6], row[7]]))
ifile.close()

o_up_uba = dict()
for o in cohort.cohort:
    key = (up[o], uba[o])
    if key in l_up_uba:
        year = cohort.cohortIntro[o].year
        o_up_uba[o] = l_up_uba[key][0][1]
        dist = abs(year-l_up_uba[key][0][0])
        for i in range(1, len(l_up_uba[key])):
            if abs(year-l_up_uba[key][0][0]) < dist:
                o_up_uba[o] = l_up_uba[key][0][1]

cohort.addData(o_up_uba, ['med.birth', 'med.sex', 'eqa', 'eqpf', 'eqr'])

cohort.addData(cohort._exitus_reason, 'exitus')
cohort.addData(cohort._exitus, 'dexitus')

from nova.variable import Variable
vnames = ['pes', 'talla', 'imc', 'tas', 'tad', 'tabac', 'alcohol', 'fa', 'fc', 'ggt', 'glucosa', 'got', 'gpt', 'urat', 'cac',
          'coltot', 'colhdl', 'colldl', 'tg', 'albuminuria', 'proteinuria24h', 'proteinuria', 'hba1c', 'creatinina', 'mdrd',
          'leucocits', 'basofils_p', 'limfocits_p', 'eosinofils_p', 'monocits_p', 'neutrofils_p', 'pcr']
l_range = {'talla' : (datetime.timedelta(-100*30), datetime.timedelta(100*30)),
           'tabac' : (datetime.timedelta(-100*30), datetime.timedelta(100*30))}

for vname in vnames:
  var = Variable(population = cohort.cohort, filename = paths.get(vname + '.txt'))
  if vname == 'tabac':
    var.ivalue = 1
    var.idate = 2
  if vname in l_range:
    l_min = l_range[vname][0]
    l_max = l_range[vname][1]
  else:
    l_min = datetime.timedelta(-12*30)
    l_max = datetime.timedelta(0)
  cohort.addData(var.lastMeasure(cohort.cohort,
                                 dfrom = misc.addToDate(cohort.cohortIntro, l_min),
                                 duntil = misc.addToDate(cohort.cohortIntro, l_max)), ['d' + vname, vname])

from nova.problems import Problems
problems = Problems(paths, problem_definition = paths.get('hyfi-problems.xml'), cmbd_path=paths.get('cmbd-ah.txt'))

probs_list = ['acute_liver_disease', 'adverse_effects', 'aff', 'ami', 'ami_atc', 'angor', 'ard', 'arthritis', 'asthma',
              'atdom', 'cataract', 'chronic_liver_disease', 'ckd', 'copd', 'dementia', 'diabetes', 'diabetes_t2',
              'dialisi', 'dvt', 'dyslipidemia', 'hepatopathy', 'hf', 'hf_atc', 'htn', 'htn_not_org', 'htn_org',
              'hyperthyroidism', 'hypothyroidism', 'ihd_acute', 'ihd_atc', 'ihd_chronic', 'miopathy', 'neoplasms_benign',
              'neoplasms_malignant', 'obesity', 'oth_arr', 'pad', 'pad_atc', 'plegies', 'sa', 'stroke', 'stroke_e', 'stroke_h', 'stroke_i',
              'sudden_death', 'tia', 'transplantament', 'vhd', 'vhd_nonrheumatic', 'vhd_rheumatic']

cond = problems.get_condition(cohort.cohort, probs_list, duntil = cohort.cohortIntro)
for p in probs_list:
  print p
  cohort.addBinaryData( cond[p], p )

meds_persistence = {'statine': 6, 'aspirine': 3, 'c10-no-statine': 6, 'a10': 3, 'b01':3,
                    'c01': 3, 'c02': 3, 'c03': 3, 'c07': 3, 'c08': 3, 'c09': 3, 'c10':3,
                    'g03a': 3, 'h02': 3, 'm01': 3,'n02': 6, 'n05': 6, 'n06': 6, 'bisphosphonate': 6}

for m in meds_persistence:
    if m not in cohort._cohortLabels:
        treatment = Treatment(population = cohort.cohort,
                              filename = paths.get(m + '.txt'),
                              catalog = paths.get('catalog'))
        treatment.iocip = 0
        treatment.iatc = 4
        treatment.ipfc = 5
        treatment.idate = 6
        treatment.ienv = 7
        cohort.addData(treatment.statusAt( cohort.cohortIntro, effect = datetime.timedelta(30 * (meds_persistence[m])) ), m)

l_incidences = ['aff', 'stroke_i', 'neoplasms_malignant', 'stroke_h', 'diabetes', 'diabetes_t2', 'hepatopathy', 'miopathy']
for l_inc in l_incidences:
  ev_inc = problems.get_events(cohort.cohort, l_inc, dfrom=cohort.cohortIntro, duntil=cohort._exitus)
  ev_inc_ecap = { o: [d for d in ev_inc['ecap'][o]] for o in set(ev_inc['ecap']) - set(ev_inc['cmbd'])}
  ev_inc_cmbd = { o: [d for d in ev_inc['cmbd'][o]] for o in set(ev_inc['cmbd']) - set(ev_inc['ecap'])}
  ev_inc_both = { o: list(set([d for d in ev_inc['ecap'][o]]) | set([d for d in ev_inc['cmbd'][o]])) for o in set(ev_inc['cmbd']) & set(ev_inc['ecap'])}
  cohort.addData({o : min(ev_inc_ecap[o]) for o in ev_inc_ecap}, 'ep_' + l_inc +'.ecap')
  cohort.addData({o : min(ev_inc_cmbd[o]) for o in ev_inc_cmbd}, 'ep_' + l_inc +'.cmbd')
  cohort.addData({o : min(ev_inc_both[o]) for o in ev_inc_both}, 'ep_' + l_inc +'.both')

from nova import writer
cohort.writeTable('data/hyfi_%s.csv' % DATE)







print '</body>'
print '</html>'
#####
